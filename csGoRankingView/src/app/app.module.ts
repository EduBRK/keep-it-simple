import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MatchComponent } from './match.component';
import { PlayerComponent } from './player.component';
import { WeaponComponent } from './weapon.component';

import { MatchService } from './match.service';
import { PlayerService } from './player.service';
import { WeaponService } from './weapon.service';

@NgModule({

  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],

  declarations: [
    AppComponent,
    MatchComponent,
    PlayerComponent,
    WeaponComponent
  ],

  providers: [
    MatchService,
    PlayerService,
    WeaponService
  ],

  bootstrap: [
    AppComponent
  ]

})

export class AppModule { }
