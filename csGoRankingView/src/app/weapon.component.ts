import { Component } from '@angular/core';
import { WeaponService } from './weapon.service';

@Component({
  selector: 'weapon',
  templateUrl: './weapon.component.html'
})

export class WeaponComponent {

  public weapons;

  constructor(private weaponService:WeaponService) {}

  ngOnInit() {

    this.weaponService.getWeaponRanking().subscribe(
      data => {this.weapons = data}
    );
    
  }

}
