import { Component } from '@angular/core';
import { MatchService } from './match.service';

@Component({
  selector: 'match',
  templateUrl: './match.component.html'
})

export class MatchComponent {

  public matches;

  constructor(private matchService:MatchService) {}

  ngOnInit() {

    this.matchService.getMatchList().subscribe(
      data => {this.matches = data}
    );
    
  }

}
