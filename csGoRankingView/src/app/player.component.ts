import { Component } from '@angular/core';
import { PlayerService } from './player.service';

@Component({
  selector: 'player',
  templateUrl: './player.component.html'
})

export class PlayerComponent {

  public players;

  constructor(private playerService:PlayerService) {}

  ngOnInit() {

    this.playerService.getPlayerRanking().subscribe(
      data => {this.players = data}
    );
    
  }

}
