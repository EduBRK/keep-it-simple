import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const baseUrl:String = 'http://localhost:8080/';

@Injectable()
export class PlayerService {

    constructor(private httpClient:HttpClient) {}

    getPlayerRanking() {
        return this.httpClient.get(baseUrl + 'getPlayerRanking');
    }

}