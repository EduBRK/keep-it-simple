import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatchComponent } from './match.component';
import { PlayerComponent } from './player.component';
import { WeaponComponent } from './weapon.component';

const routes: Routes = [
    { path: '', redirectTo: '/match', pathMatch: 'full' },
    { path: 'match', component: MatchComponent },
    { path: 'player', component: PlayerComponent },
    { path: 'weapon', component: WeaponComponent }    
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}