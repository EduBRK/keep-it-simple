Teste prático Java Keep It Simple.

Desenvolvido em Java 8 e Angular 5.

Teste: 

    Introdução 
    
    Você terá que construir uma aplicação que implementa uma série de funcionalidades descritas abaixo.  
    Não é necessário implementar o front end, seu objetivo é expor os serviços RESTFul para tais funcionalidades. 
    A comunicação deve ser através de JSON sobre HTTP. 
    A entrega do projeto será por e-mail, encaminhe seu projeto para ffa@keepsimple.com.br. 
    Utilizar as seguintes tecnologias para desenvolvimento do teste:  
    
    ● Java 8; 
    ● Spring Boot; 
    ● Spring Data JPA;

    Observações: 
        1. Você terá que carregar os dados JSON dos arquivos fornecidos. 
        2. Levaremos em consideração a organização do projeto, design patterns e boas práticas. 
        3. Log de sistema e comentários são sempre bem-vindos. 
        4. Não é obrigatório o uso de banco de dados (pode ser criado um datasource para o arquivo).  
        5. Uma avaliação importante deste teste é a capacidade de entendimento e tomada de decisões de implementação. Se alguma informação 
        não estiver clara, tome uma decisão considerando todas as informações aqui contidas e justifique com um comentário no código.

    Funcionalidades  

    Nós estamos criando um ranking sistema de histórico e ranqueamento de partidas FFA de CS:GO disputadas entre jogadores VIPs. 
    Inicialmente foram colhidas as seguintes necessidades junto aos representantes dos usuários chave:  

    1. Carregar histórico de partidas 
    Eu, como administrador do ranking, preciso carregar o histórico de partidas e kills já realizados, 
    conforme os arquivos disponibilizados. Não é necessário disponibilizar este serviço na API. 

    2. Ranking de jogadores 
    Eu, como jogador e fã, gostaria de visualizar o ranking de jogadores, com números de kills realizados, 
    mortes sofridas e pontuação (=kills – mortes), podendo filtrar pelas datas das partidas jogadas.  

    3. Registrar o Kill 
    Eu, como mantenedor do ranking, preciso registrar o resultado dos confrontos, onde cada kill recebido estará 
    acompanhado do jogador que realizou o kill, a arma utilizado e o jogador que morreu, recebendo esta informação do servidor de jogos.  

    4. Ranking de Armas 
    Eu, como jogador e fã, gostaria de saber o ranking das armas com mais kills, tanto o ranking geral 
    de todos os tempos, quanto por um período de tempo.  
    
    Atividades extras (opcional)   
    
    5. Lista de Partidas 
    Eu, como jogador e fã, gostaria de ter uma lista de partidas, onde possa ver a data e hora de início e 
    fim da partida, a quantidade de jogadores e o jogador campeão, aquele com maior pontuação (=kills – mortes).  

    6. Detalhe das Partidas 
    Eu, como jogador e fã, gostaria de escolher e visualizar os detalhes de uma partida, com o ranking dos 
    jogadores da partida (pontuação) e a data e hora de início e fim. Escolherei uma partida através da lista de partidas.