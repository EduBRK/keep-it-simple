package br.org.keep.csgo.service;

import br.org.keep.csgo.model.Match;
import br.org.keep.csgo.view.objects.PlayerRanking;
import br.org.keep.csgo.view.objects.WeaponRanking;
import br.org.keep.csgo.model.Wrangle;

import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Interface com os métodos criados para manipular Wrangles.
 *
 * OBS: Não criei uma Interface de services genéricas, como um CRUD, por não haver necessidade nesse exemplo, mesmo
 * sendo padrão criarmos no mercado em geral para aplicações empresariais.
 */
public interface WrangleService {

    /**
     * Persiste uma lista de {@link Wrangle}.
     *
     * @param wrangles Lista de {@link Wrangle} a serem persistidos.
     */
    void save(List<Wrangle> wrangles);

    /**
     * Persiste uma entidade de {@link Wrangle}
     *
     * @param wrangle {@link Wrangle} a ser persitido.
     * @return Wrangle persistido.
     */
    Wrangle save(Wrangle wrangle);

    /**
     * Recupera a lista de Weapons com seus rankings do banco de dados.
     *
     * @return Lista de {@link WeaponRanking}, que armazena os dados extras que são retornados por esse método.
     */
    List<WeaponRanking> getWeaponRanking(Date matchDate);

}
