package br.org.keep.csgo.jackson.deserializer;

import br.org.keep.csgo.model.Match;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

/**
 * @author Eduardo Augusto
 *
 * Classe para deserializar o objeto {@link Match} do arquivo wrangle.json.
 */
public class MatchWrangleDeserializer extends StdDeserializer<Match> {

    public MatchWrangleDeserializer() {
        this(null);
    }

    public MatchWrangleDeserializer(Class<?> clazz) {
        super(clazz);
    }

    @Override
    public Match deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

        JsonNode jsonNode = jsonParser.getCodec().readTree(jsonParser);

        Match match = new Match();
        match.setMatch(jsonNode.longValue());

        return match;

    }
}
