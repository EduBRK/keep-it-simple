package br.org.keep.csgo.view.objects;

import java.sql.Timestamp;

/**
 * @author Eduardo Augusto
 *
 * View Object para retornar os dados de maneira mais ordenada como resposta das consultas do Match.
 */
public class MatchData {

    private Long match;
    private Timestamp beginTime;
    private Timestamp endTime;
    private Integer playerCount;
    private String mvp;

    public MatchData(Object[] object, Object mvp) {
        this.match = (Long) object[0];
        this.beginTime = (Timestamp) object[1];
        this.endTime = (Timestamp) object[2];
        this.playerCount = (Integer) object[3];
        this.mvp = (String) mvp;
    }

    public Long getMatch() {
        return match;
    }

    public void setMatch(Long match) {
        this.match = match;
    }

    public Timestamp getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Timestamp beginTime) {
        this.beginTime = beginTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Integer getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(Integer playerCount) {
        this.playerCount = playerCount;
    }

    public String getMvp() {
        return mvp;
    }

    public void setMvp(String mvp) {
        this.mvp = mvp;
    }
}
