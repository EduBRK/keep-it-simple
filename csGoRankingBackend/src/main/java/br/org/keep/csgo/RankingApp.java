package br.org.keep.csgo;

import br.org.keep.csgo.model.Match;
import br.org.keep.csgo.model.Wrangle;
import br.org.keep.csgo.service.MatchService;
import br.org.keep.csgo.service.WrangleService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Classe principal da aplicação Springboot.
 * Responsável por iniciar a aplicação e importar os dados para o banco de dados H2.
 */
@EntityScan(
    basePackageClasses = {RankingApp.class, Jsr310JpaConverters.class}
)
@SpringBootApplication
public class RankingApp {

    /**
     * Inicia a aplicação Springboot.
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(RankingApp.class, args);
    }

    /**
     * CommandLineRunner que importa os dados dos arquivos JSON para o banco de dados H2.
     *
     * @param matchService Service que irá persistir os dados referentes ao arquivo matches.json
     * @param wrangleService Service que irá persistir os dados referentes ao arquivo wrangle.json
     * @return args Lambda que será executado durante o método run do {@link SpringApplication}
     */
    @Bean
    CommandLineRunner runner(MatchService matchService, WrangleService wrangleService) {

        return args -> {

            // Registrando módulos com suporte a nova API de datas do Java 8
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.findAndRegisterModules();

            // Criando referência para deserialização
            TypeReference<List<Match>> matchTypeReference = new TypeReference<List<Match>>() {};
            TypeReference<List<Wrangle>> wrangleTypeReference = new TypeReference<List<Wrangle>>() {};

            // Carregando arquivos
            InputStream matchInputStream = TypeReference.class.getResourceAsStream("/fixed_matches.json");
            InputStream wrangleInputStream = TypeReference.class.getResourceAsStream("/fixed_wrangle.json");

            try {

                List<Match> matches = objectMapper.readValue(matchInputStream, matchTypeReference);
                List<Wrangle> wrangles = objectMapper.readValue(wrangleInputStream, wrangleTypeReference);

                matchService.save(matches);
                wrangleService.save(wrangles);

            } catch (IOException e) {
                // Como a aplicação está sendo executada somente local, como uma POC, um printStackTrace é o suficiente
                // no tempo disponível.
                e.printStackTrace();
            }

        };
    }

}
