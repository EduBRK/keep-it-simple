package br.org.keep.csgo.service.implementation;

import br.org.keep.csgo.model.Match;
import br.org.keep.csgo.view.objects.MatchData;
import br.org.keep.csgo.repository.MatchRepository;
import br.org.keep.csgo.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Implementação do {@link MatchService}
 */
@Service
public class MatchServiceImplementation implements MatchService {

    @Autowired
    private MatchRepository matchRepository;

    @Override
    public void save(List<Match> matches) {
        matchRepository.save(matches);
    }

    @Override
    public List<MatchData> getMatchList() {

        List<Object[]> matches = matchRepository.getMatchList();
        List<MatchData> matchDataList = new ArrayList<>();

        for (int i = 0; i < matches.size(); i++) {
            Object mvp = matchRepository.getTopByMatch((Long) matches.get(i)[0]).get(0);
            matchDataList.add(new MatchData(matches.get(i), mvp));
        }

        return matchDataList;

    }

}
