package br.org.keep.csgo.service.implementation;

import br.org.keep.csgo.model.Wrangle;
import br.org.keep.csgo.repository.WrangleRepository;
import br.org.keep.csgo.service.WrangleService;
import br.org.keep.csgo.view.objects.WeaponRanking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Implementação do {@link WrangleService}
 */
@Service
public class WrangleServiceImplementation implements WrangleService {

    @Autowired
    private WrangleRepository wrangleRepository;

    @Override
    public void save(List<Wrangle> wrangles) {
        wrangleRepository.save(wrangles);
    }

    @Override
    public Wrangle save(Wrangle wrangle) {
        return wrangleRepository.save(wrangle);
    }

    @Override
    public List<WeaponRanking> getWeaponRanking(Date matchDate) {

        List<Object[]> result = wrangleRepository.getWeaponRanking(matchDate);
        List<WeaponRanking> weaponRankingList = new ArrayList<>();

        for (Object[] object: result) {
            weaponRankingList.add(new WeaponRanking(object));
        }

        return weaponRankingList;

    }
}
