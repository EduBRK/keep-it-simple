package br.org.keep.csgo.controllers;

import br.org.keep.csgo.model.*;
import br.org.keep.csgo.service.MatchService;
import br.org.keep.csgo.service.PlayerService;
import br.org.keep.csgo.service.WrangleService;
import br.org.keep.csgo.view.objects.MatchData;
import br.org.keep.csgo.view.objects.PlayerRanking;
import br.org.keep.csgo.view.objects.WeaponRanking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * {@link RestController} responsável por todos os acesso da aplicação.
 */
@RestController
public class RankingController {

    @Autowired
    private MatchService matchService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private WrangleService wrangleService;

    /**
     * Recupera uma lista de jogadores, com seu ranking, deaths, kills e saldo de pontos.
     * @param matchDate data inicial da pesquisa da partida.
     * @return Lista de {@link PlayerRanking}, com todos os dados necessários, ordenados pelio ranking do mesmo.
     */
    @GetMapping("getPlayerRanking")
    public List<PlayerRanking> getPlayerRanking(@RequestParam(name="matchDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date matchDate) {
        return playerService.getPlayerRanking(matchDate);
    }

    /**
     * Grava um {@link Wrangle} no banco de dados, os dados da kill registrada.
     *
     * OBS: Os testes foram realizados passando por parâmetro um {@link Wrangle} modificado contido no arquivo wrangle.json. É necessário que o ID
     * da partida exista para o save funcione corretamente sem sujar a base com uma partida sem data de início e fim e sem jogadores. Seria necessário
     * criar também o CRUD para as partidas para que esse POST ficasse 100% funcional.
     *
     * @param wrangle Wrangle com os dados do Kill.
     * @return Wrangle que for persistido na base de dados.
     */
    @PostMapping("registerKill")
    public Wrangle registerKill(@RequestBody Wrangle wrangle) {
        return wrangleService.save(wrangle);
    }

    /**
     * Recupera a lista de armas por Ranking de Kills.
     * @param matchDate data inicial de pesquisa dos kills.
     * @return Lista de {@link WeaponRanking} com o nome da arma e quantidade de kills
     */
    @GetMapping("getWeaponRanking")
    public List<WeaponRanking> getWeaponRanking(@RequestParam(name="matchDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date matchDate) {
        return wrangleService.getWeaponRanking(matchDate);
    }

    /**
     * Retorna uma lista com os dados por partida, com o horário de ínicio e fim, além da contagem de players e quem foi o
     * mvp da partida.
     * @return Lista de partidas em um objeto {@link MatchData}
     */
    @GetMapping("getMatchList")
    public List<MatchData> getMatchList() {
        return matchService.getMatchList();
    }

}
