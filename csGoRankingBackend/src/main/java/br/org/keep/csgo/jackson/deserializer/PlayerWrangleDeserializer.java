package br.org.keep.csgo.jackson.deserializer;

import br.org.keep.csgo.model.Player;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

/**
 * @author Eduardo Augusto
 *
 * Classe para deserializar o objeto {@link Player} do arquivo wrangle.json.
 */
public class PlayerWrangleDeserializer extends StdDeserializer<Player> {

    public PlayerWrangleDeserializer() {
        this(null);
    }

    public PlayerWrangleDeserializer(Class<?> clazz) {
        super(clazz);
    }

    @Override
    public Player deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

        JsonNode jsonNode = jsonParser.getCodec().readTree(jsonParser);

        Player player = new Player();
        player.setName(jsonNode.textValue());

        return player;

    }

}
