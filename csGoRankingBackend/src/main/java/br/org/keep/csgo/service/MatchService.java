package br.org.keep.csgo.service;

import br.org.keep.csgo.model.Match;
import br.org.keep.csgo.view.objects.MatchData;

import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Interface com os métodos criados para manipular Matches.
 *
 * OBS: Não criei uma Interface de services genéricas, como um CRUD, por não haver necessidade nesse exemplo, mesmo
 * sendo padrão criarmos no mercado em geral para aplicações empresariais.
 */
public interface MatchService {

    /**
     * Persiste uma lista de {@link Match}.
     *
     * @param matches Lista de {@link Match} a serem persistidos.
     */
    void save(List<Match> matches);

    /**
     * Recupera a lista de Matches do banco de dados.
     *
     * @return Lista de {@link MatchData}, que armazena os dados extras que são retornados por esse método.
     */
    List<MatchData> getMatchList();

}
