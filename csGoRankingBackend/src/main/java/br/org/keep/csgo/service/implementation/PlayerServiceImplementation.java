package br.org.keep.csgo.service.implementation;

import br.org.keep.csgo.repository.PlayerRepository;
import br.org.keep.csgo.service.PlayerService;
import br.org.keep.csgo.view.objects.PlayerRanking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Implementação do {@link PlayerService}
 */
@Service
public class PlayerServiceImplementation implements PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    @Override
    public List<PlayerRanking> getPlayerRanking(Date matchDate) {

        List<Object[]> result = playerRepository.getPlayerRanking(matchDate);
        List<PlayerRanking> playerRankingList = new ArrayList<>();

        for (Object[] object: result) {
            playerRankingList.add(new PlayerRanking(object));
        }

        return playerRankingList;

    }

}
