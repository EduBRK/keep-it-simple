package br.org.keep.csgo.model.keys;

import br.org.keep.csgo.jackson.deserializer.MatchWrangleDeserializer;
import br.org.keep.csgo.model.Match;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalTime;

/**
 * @author Eduardo Augusto
 *
 * Classe que representa a chave embebida da entidade {@link br.org.keep.csgo.model.Wrangle}
 */
@Embeddable
public class WrangleKey implements Serializable {

    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JsonDeserialize(using = MatchWrangleDeserializer.class)
    private Match match;
    private LocalTime killtime;

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public LocalTime getKilltime() {
        return killtime;
    }

    public void setKilltime(LocalTime killtime) {
        this.killtime = killtime;
    }

}
