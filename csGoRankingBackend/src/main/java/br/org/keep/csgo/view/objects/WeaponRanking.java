package br.org.keep.csgo.view.objects;

/**
 * @author Eduardo Augusto
 *
 * View Object para retornar os dados de maneira mais ordenada como resposta das consultas com o Ranking das Weapons.
 */
public class WeaponRanking {

    public String weapon;
    public Long kills;

    public WeaponRanking(Object[] object) {
        this.weapon = (String) object[0];
        this.kills = (Long) object[1];
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public Long getKills() {
        return kills;
    }

    public void setKills(Long kills) {
        this.kills = kills;
    }

}
