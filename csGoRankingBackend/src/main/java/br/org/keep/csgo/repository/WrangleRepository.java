package br.org.keep.csgo.repository;

import br.org.keep.csgo.model.Wrangle;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Repository com as pesquisas realizadas com a entidade Wrangle.
 */
@Repository
public interface WrangleRepository extends CrudRepository<Wrangle, Serializable> {

    /**
     * Recupera a lista de armas, agrupadas pelo seu nome, com a contagem de vezes que a arma foi utilizada para um kill.
     *
     * @param matchDate Data Inicial de quando o ranking será pesquisado.
     * @return Lista de Objects, com cada um dos dados necessários. Deve ser convertido em outra entidade pelos services.
     */
    @Query("SELECT W.weapon, COUNT(W.weapon) AS Kills FROM Wrangle W, Match M " +
            "WHERE W.wrangleKey.match.match = M.match AND M.begin >= COALESCE(:matchDate, M.begin) " +
            "GROUP BY W.weapon HAVING COUNT(W.weapon) > 0 ORDER BY Kills DESC ")
    List<Object[]> getWeaponRanking(@Param("matchDate") Date matchDate);

}