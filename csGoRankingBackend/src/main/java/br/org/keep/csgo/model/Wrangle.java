package br.org.keep.csgo.model;

import br.org.keep.csgo.jackson.deserializer.PlayerWrangleDeserializer;
import br.org.keep.csgo.model.keys.WrangleKey;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Eduardo Augusto
 *
 * Classe que representa a entidade Wrangle.
 */
@Entity
public class Wrangle implements Serializable {

    @EmbeddedId
    @JsonUnwrapped
    private WrangleKey wrangleKey;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JsonDeserialize(using = PlayerWrangleDeserializer.class)
    private Player killer;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JsonDeserialize(using = PlayerWrangleDeserializer.class)
    private Player killed;

    private String weapon;

    public WrangleKey getWrangleKey() {
        return wrangleKey;
    }

    public void setWrangleKey(WrangleKey wrangleKey) {
        this.wrangleKey = wrangleKey;
    }

    public Player getKiller() {
        return killer;
    }

    public void setKiller(Player killer) {
        this.killer = killer;
    }

    public Player getKilled() {
        return killed;
    }

    public void setKilled(Player killed) {
        this.killed = killed;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

}
