package br.org.keep.csgo.view.objects;;

/**
 * @author Eduardo Augusto
 *
 * View Object para retornar os dados de maneira mais ordenada como resposta das consultas de Players.
 */
public class PlayerRanking {

    private String playerName;
    private Long deaths;
    private Long kills;
    private Long score;

    public PlayerRanking(Object[] object) {
        this.playerName = (String) object[0];
        this.deaths = (Long) object[1];
        this.kills = (Long) object[2];
        this.score = (Long) object[3];
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Long getDeaths() {
        return deaths;
    }

    public void setDeaths(Long deaths) {
        this.deaths = deaths;
    }

    public Long getKills() {
        return kills;
    }

    public void setKills(Long kills) {
        this.kills = kills;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

}
