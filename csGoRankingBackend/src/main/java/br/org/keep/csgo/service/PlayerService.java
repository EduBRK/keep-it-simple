package br.org.keep.csgo.service;

import br.org.keep.csgo.view.objects.PlayerRanking;

import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Interface com os métodos criados para manipular Players.
 *
 * OBS: Não criei uma Interface de services genéricas, como um CRUD, por não haver necessidade nesse exemplo, mesmo
 * sendo padrão criarmos no mercado em geral para aplicações empresariais.
 */
public interface PlayerService {

    /**
     * Recupera a lista de Players com seus rankings do banco de dados.
     *
     * @return Lista de {@link PlayerRanking}, que armazena os dados extras que são retornados por esse método.
     */
    List<PlayerRanking> getPlayerRanking(Date matchDate);

}
