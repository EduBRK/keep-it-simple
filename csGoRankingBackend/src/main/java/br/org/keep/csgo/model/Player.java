package br.org.keep.csgo.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Eduardo Augusto
 *
 * Classe que representa a entidade Player.
 */
@Entity
public class Player implements Serializable {

    @Id
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
