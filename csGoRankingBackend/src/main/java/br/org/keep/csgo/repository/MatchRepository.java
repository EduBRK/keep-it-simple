package br.org.keep.csgo.repository;

import br.org.keep.csgo.model.Match;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Repository com as pesquisas realizadas com a entidade Match.
 */
@Repository
public interface MatchRepository extends CrudRepository<Match, Serializable> {

    /**
     * Recupera uma lista de Matches juntamente a quantidade de jogadores que participaram dessa Match.
     *
     * @return lista de Objects com os atributos necessários.
     */
    @Query("SELECT M.match, M.begin, M.end, SIZE(M.players) FROM Match M GROUP BY M.match")
    List<Object[]> getMatchList();

    /**
     * Recupera a lista jogadores ordenados pela quantidade de kills que o jogador teve naquela match de maneira decrescente.
     *
     * @param match {@link Long} com o número da match.
     * @return Lista de {@link String} com os nomes dos jogadores.
     */
    @Query("SELECT W.killer.name FROM Wrangle W WHERE W.wrangleKey.match.match = :match " +
            "GROUP BY W.killer.name HAVING COUNT(*) > 0 ORDER BY W.killer.name DESC")
    List<String> getTopByMatch(@Param("match") Long match);

}