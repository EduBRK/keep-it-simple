package br.org.keep.csgo.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Classe que representa a entidade Match.
 */
@Entity
public class Match implements Serializable {

    @Id
    private Long match;

    private Timestamp begin;
    private Timestamp end;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Player> players;

    public Long getMatch() {
        return match;
    }

    public void setMatch(Long match) {
        this.match = match;
    }

    public Timestamp getBegin() {
        return begin;
    }

    public void setBegin(Timestamp begin) {
        this.begin = begin;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

}