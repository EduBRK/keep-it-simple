package br.org.keep.csgo.repository;

import br.org.keep.csgo.model.Player;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Augusto
 *
 * Repository com as pesquisas realizadas com a entidade Player.
 */
@Repository
public interface PlayerRepository extends CrudRepository<Player, Serializable> {

    /**
     * Recupera uma lista com todos os Players, com a contagem de kills, de deaths e seu score (kills - deaths),
     * ordenados descrescentemente por seu score.
     *
     * @param matchDate Data Inicial de quando o ranking será pesquisado.
     * @return Lista de Objects, com cada um dos dados necessários. Deve ser convertido em outra entidade pelos services.
     */
    @Query("SELECT P.name as player, " +
            "COALESCE((SELECT COUNT(*) FROM Wrangle W, Match M  " +
            "WHERE W.killer.name = P.name and W.wrangleKey.match.match = M.match " +
            "and M.begin >= COALESCE(:matchDate, M.begin) GROUP BY W.killer.name), 0), " +
            "COALESCE((SELECT COUNT(*) FROM Wrangle W, Match M  " +
            "WHERE W.killed.name = P.name and W.wrangleKey.match.match = M.match " +
            "and M.begin >= COALESCE(:matchDate, M.begin) GROUP BY W.killed.name), 0), " +
            "COALESCE(" +
            "(SELECT COUNT(*) FROM Wrangle W, Match M " +
            "WHERE W.killer.name = P.name and W.wrangleKey.match.match = M.match " +
            "and M.begin >= COALESCE(:matchDate, M.begin) GROUP BY W.killer.name) -  " +
            "(SELECT COUNT(*) FROM Wrangle W, Match M " +
            "WHERE W.killed.name = P.name and W.wrangleKey.match.match = M.match " +
            "and M.begin >= COALESCE(:matchDate, M.begin) GROUP BY W.killed.name), 0)" +
            " as score " +
            "FROM Player P " +
            "ORDER BY score DESC ")
    List<Object[]> getPlayerRanking(@Param("matchDate") Date matchDate);

}
